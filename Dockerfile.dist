#
# Dockerfile
#
# Copyright (C) 2020-2021 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This file is part of docker-debian-postgis-django.
#
# docker-debian-postgis-django is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# docker-debian-postgis-django is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with docker-debian-postgis-django.  If not, see <http://www.gnu.org/licenses/>.
#

FROM python:3.9.9-bullseye AS base

# Pass the running user and group.
ARG UID
ARG GID
ARG PYPI_MIRROR

RUN groupadd -g $GID -r django \
    && useradd -l --create-home -s /bin/bash --home-dir /code -u $UID -r -g django django \
    && mkdir /code/django \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        gcc=4:10.2.1-1 \
        python3-dev=3.9.2-3 \
        libpq-dev=13.5-0+deb11u1 \
        graphviz=2.42.2-5 \
        libgraphviz-dev=2.42.2-5 \
        gettext=0.21-4 \
        postgresql-client-13=13.5-0+deb11u1 \
        postgis=3.1.1+dfsg-1 \
    && rm -rf /var/cache/apt /var/lib/apt/lists/* \
    && chmod 700 /code \
    && chown django:django /code \
    && chown django:django /code/django

COPY --chown=django:django ./utils/ /code/django/utils

# Install pipenv
USER django:django
RUN pip3 install --user --no-cache-dir --index-url ${PYPI_MIRROR} pipenv==2021.11.23
