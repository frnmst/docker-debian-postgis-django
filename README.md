# docker-debian-postgis-django

An image to be used for the first stage of Django projects that use PostGIS and Debian.

## Table of contents

<!--TOC-->

- [docker-debian-postgis-django](#docker-debian-postgis-django)
  - [Table of contents](#table-of-contents)
  - [Description](#description)
    - [Database](#database)
  - [Import in another project](#import-in-another-project)
    - [Build](#build)
      - [Build the image directly](#build-the-image-directly)
      - [Build via git](#build-via-git)
    - [Dockerfile](#dockerfile)
    - [Continuous integration](#continuous-integration)
    - [Other files](#other-files)
      - [Variables](#variables)
  - [Dependencies](#dependencies)
    - [Dockerfile](#dockerfile-1)
      - [Version pinning](#version-pinning)
  - [License](#license)
  - [Changelog and trusted source](#changelog-and-trusted-source)
  - [Crypto donations](#crypto-donations)

<!--TOC-->

## Description

This repository contains a dockerfile and a compose file used to install some basic
system dependencies for Django, PostgreSQL and PostGIS.

### Database

You have various options here. The only requirement is that you use PostgreSQL with
the PostGIS extension enabled.

These are the tested images:

| Image name | Full image name | Repository | docker-debian-postgis-django versions |
|------------|-----------------|------------|---------------------------------------|
| [kartoza/postgis](https://hub.docker.com/r/kartoza/postgis/) | kartoza/postgis:12.1 | https://github.com/kartoza/docker-postgis | `0.0.1`, `0.0.2` |
| [postgis/postgis](https://hub.docker.com/r/postgis/postgis/) | postgis/postgis:12-3.0-alpine | https://github.com/postgis/docker-postgis | `0.0.1`, `0.0.2` |
| [postgis/postgis](https://hub.docker.com/r/postgis/postgis/) | postgis/postgis:13-3.1 | https://github.com/postgis/docker-postgis | >= `0.0.4` |

## Import in another project

### Build

#### Build the image directly

1. clone this repository
2. run:

   ```shell
   cp Dockerfile.dist Dockerfile
   docker-compose build  --build-arg GID=$(id -g) --build-arg UID=$(id -u) --build-arg PYPI_MIRROR=https://pypi.org/simple
   ```

3. add this to the docker compose file of your project:

   ```yaml
   services:
       dependencies:
           image: docker_debian_postgis_django

       # The main service of you project.
       main:

           ...

           depends_on:
               - dependencies
               - ...
   ```

#### Build via git

1. add this to the docker compose file of your project, where `${VERSION}` may correspond to
   a git tag such as `0.0.3`:

   ```yaml
   services:

       dependencies:
           image: docker_debian_postgis_django
           build:
               context: https://software.franco.net.eu.org/frnmst/docker-debian-postgis-django.git#${VERSION}
               dockerfile: Dockerfile.dist

       # The main service of you project.
       main:

           ...

           depends_on:
               - dependencies
               - ...
   ```

### Dockerfile

Add this line as the first instruction in the Dockerfile:

    FROM docker_debian_postgis_django as builder

Add this as the the last `COPY` instruction in the Dockerfile:

    COPY --from=docker_debian_postgis_django /code/utils /code/utils

### Continuous integration

[Jenkins](https://jenkins.io) is used for continuous integration.

Have a look at the implementation in
[django-futils](https://software.franco.net.eu.org/frnmst/django-futils/raw/branch/master/Jenkinsfile)
for an example to get reproducible build for development and production environments.

**Warning: some files are automatically replaced when you run the Jenkins pipeline, such as `SECRET_SETTINGS.py`. Make sure to run the pipeline in a separate environment.**

### Other files

- `Makefile.dist`: use all setup operations without typing them manually
- `uwsgi.ini.dist`: run uwsgi

You can put these files in your repository and rename them into
`Makefile` and `uwsgi.ini`.
[django-futils](https://software.franco.net.eu.org/frnmst/django-futils),
for example, uses this method.

#### Variables

To be able to call `make` you must create a `.env` file in the project root with some variables.
See for example the ones used in [django-futils](https://software.franco.net.eu.org/frnmst/django-futils/raw/branch/master/env.dist).

## Dependencies

### Dockerfile

These dependencies are installed with the `RUN` command so there is no need to install them manually.

| Software                                         | Dependency name      | Purpose                                |
|--------------------------------------------------|----------------------|----------------------------------------|
| [gettext](https://www.gnu.org/software/gettext/) | gettext              | translations                           |
| [Graphviz](https://www.graphviz.org/)            | graphviz             | database schema graph                  |
| [Graphviz](https://www.graphviz.org/)            | libgraphviz-dev      | database schema graph                  |
| [Postgis](https://postgis.net/)                  | postgis              | postgres extension                     |
| [PostgreSQL](https://www.postgresql.org/)        | postgresql-client    | poll database availability with `psql` |

#### Version pinning

Version pinning should improve reproducibility. Since version `0.0.3` the Dockerfile in this project
uses pinned Debian packages.

If you have a Debian installation you can run `# apt-get update && apt policy ${package_name}` to find
out the current software versions of the dependencies.

## License

Copyright (C) 2020-2021 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)

docker-debian-postgis-django is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

docker-debian-postgis-django is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with docker-debian-postgis-django.  If not, see <http://www.gnu.org/licenses/>.

## Changelog and trusted source

You can check the authenticity of new releases using my public key.

Changelogs, instructions, sources and keys can be found at [blog.franco.net.eu.org/software](https://blog.franco.net.eu.org/software/).

## Crypto donations

- Bitcoin: bc1qnkflazapw3hjupawj0lm39dh9xt88s7zal5mwu
- Monero: 84KHWDTd9hbPyGwikk33Qp5GW7o7zRwPb8kJ6u93zs4sNMpDSnM5ZTWVnUp2cudRYNT6rNqctnMQ9NbUewbj7MzCBUcrQEY
- Dogecoin: DMB5h2GhHiTNW7EcmDnqkYpKs6Da2wK3zP
- Vertcoin: vtc1qd8n3jvkd2vwrr6cpejkd9wavp4ld6xfu9hkhh0
